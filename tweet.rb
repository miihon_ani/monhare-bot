require 'rubygems'
require 'twitter'

class Tweet
  def initialize
    @text = <<-EOF.split("\n")
カトレア
フェイ
タバサ
ミーティア
マチルダ
サンカ
ラクリマ
ユエル
イノセント
ラーネス
ホワイトリリー
カーリス
ラザニア
エヴェリーナ
シーナ
ミル
イース
Ｋ－８
ヴェロニカ
フィリア
エイス
EOF
    @client = Twitter::REST::Client.new do |config|
      if !ENV['CONSUMER_KEY'] then
        # 以下、.envファイルとか環境変数からAPIKeyを取る奴
        require_relative './envloader.rb'
        Envloader.load
      end
      config.consumer_key        = ENV['CONSUMER_KEY']
      config.consumer_secret     = ENV['CONSUMER_SECRET']
      config.access_token        = ENV['ACCESS_TOKEN']
      config.access_token_secret = ENV['ACCESS_TOKEN_SECRET']
    end
  end
  def random_tweet
    tweet = @text[rand(@text.length)]
    update(tweet)
  end
  def daily_tweet
    tweet = @text[Time.now.day - 1]
    update(tweet)
  end
  def test_tweet
    update('なんなんですかー！？')
  end
  private
  def update(tweet)
    return nil unless tweet
    begin
      t = Time.now + (60 * 60 * 9) # 自力でタイムゾーン分足してる
      @client.update('(' << t.strftime('%H:%M:%S') << ')' << tweet.chomp)
    rescue => ex
      nil # todo
    end
  end
end

# ローカルからのテスト用 以下のコマンドで実行される
# $ruby tweet.rb
if __FILE__ == $0
  Tweet.new.random_tweet
end
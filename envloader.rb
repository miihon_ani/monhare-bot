class Envloader
  class << self
    def load(filepath = File.expand_path('../.env', __FILE__))
      File.open(filepath) do |f|
        f.each_line do |line|
          if line =~ /\=/
            key, value = line.split('=').map(&:strip)
            value = value.match(/[\'\"]?([^\'\"]+)[\'\"]?/)[0] # 引用符を削除
            ENV[key] = value
          end
        end
      end
    end
  end
end